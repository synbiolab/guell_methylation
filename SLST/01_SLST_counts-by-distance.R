args = commandArgs(trailingOnly=TRUE)

library(ShortRead)
library(Biostrings)
library(DECIPHER)
library(plyr)

R1 = args[1]
R2 = args[2]
sample = args[3]

SLST_sequences <- readFasta("SLST_Allels.fa")
result_path = "./" 
  
##### The amplicon is too long to be fully sequenced. We will have the first 223 nt and the last 207. Split between distinguishable and not distinguishable sequences 
SLST_sequenced_part <- lapply(sread(SLST_sequences), function(x) { DNAString(paste0(substr(x, 1, 223), substr(x, nchar(x)-209+1+2, nchar(x)) )) }) #Gap of 56nts

##### Reads sequences 
R1_file <- readFastq(R1)
R2_file <- readFastq(R2)
R1_seq <- sread(R1_file)
R2_seq <- sread(R2_file)
R2_seq <- reverseComplement(R2_seq)
concat_seqs <- DNAStringSet(paste0(as.character(R1_seq), as.character(R2_seq)))
concat_seqs <- substr(concat_seqs, 29, nchar(concat_seqs)-44) ## There are 28 and 44 nucleotides not present in SLST sequences (out of the alignment)

#####  Get easily comparable sequences (of the same length), calculate the distance and save the strains with minimal distance
result <- ldply(c(1:length(concat_seqs)),.fun = function(x){
  dist <- DistanceMatrix(DNAStringSet(append(SLST_sequenced_part, DNAString(concat_seqs[x]))))
  sim_strains <- which(dist[163,1:162] == min(dist[163,1:162]))
  strains_name <- as.character(ShortRead::id(SLST_sequences)[sim_strains])
  c(x, paste(strains_name, collapse = '-'), min(dist[163,1:162]))
  }
  )

write.csv(result, paste0(result_path, sample, "_counts-by-distance.csv"))


