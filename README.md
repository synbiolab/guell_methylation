# Engineering selectivity of Cutibacterium acnes phages by epigenetic imprinting #

### Nastassia Knödlseder (1), Guillermo Nevot (1), Maria-José Fábrega (1), Julia Mir-Pedrol (1), Marta Sanvicente-García (1), Nil Campamà-Sanz (1), Rolf Lood (2), Marc Güell (1)** ###

(1) Department of Experimental and Health Sciences, Pompeu Fabra University, Barcelona, Spain
(2) Division of Infection Medicine, Department of Clinical Sciences, Faculty of Medicine, Lund, Sweden

** Correspondence should be addressed to marc.guell@upf.edu 

This repository contains the analysis scripts:


* meth_analysis.R contains the custom plotting of all tombo methylation data obtained. 
* SLST/01_SLST_counts-by-distance.R calculates Hamming distance between sequencing data and SLST alleles
* SLST/02_SLST_clusters.R clusters sequencing data counts into clades 
* SLST/03_SLST_plot.R plots of clade proportions found in analysed samples 


